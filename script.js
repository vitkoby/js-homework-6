/* Ответы на теоритические вопросы:
1. Для экранирования мы используем "\" - обратный слэш. При экранировании слэши в консоли не 
отображаются. Используется к примеру когда во внутрь строки надо вставить к примеру кавычку одного вида с 
кавычками для самой строки. 
2. Function declaration, Function expression, Стрелочная функция
3. Hoisting или поднятие -  это механизм в JavaScript, в котором переменные и объявления функций, 
передвигаются вверх своей области видимости перед тем, как код будет выполнен. */



function createNewUser() {
    let personName = prompt('Введите свое имя');
    let personLastName = prompt('Введите свою фамилию');
    let personAge = prompt('Введите Ваш возраст');
    let birthday = new Date(prompt('Введите дату вашего рождения', 'dd.mm.yyyy'));
    const newUser = {
        firstName: personName,
        lastName: personLastName,
        birthYear: birthday.getFullYear(),
        age: personAge,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getbirthYear() {
            return (this.birthYear);
        },
        getPassword() {
            return (this.firstName[0].toUpperCase()) + (this.lastName.toLowerCase()) + (this.birthYear);
        },
        getAge() {
            return (this.age);
        },
    };
    return newUser;
}

const user = createNewUser();
/* console.log(createNewUser()); */
console.log(user.getAge());
console.log(user.getPassword());


